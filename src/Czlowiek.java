public class Czlowiek {

    short wiek;
    String imie;
    String nazwisko;
    int wzrost; //stworzenie zmiennych


    //TWORZENIE METOD do tych zmiennych;


    // pisanie jawne konstruktora: - opisałem to w klasie Main służy do konstruowania obiektów danej klasy
    //Czlowiek(){


Czlowiek() {
    
}



    public void setImie (String imie) { // ustawianie metod set
        this.imie = imie; // this służy do odrózniania czy chodzi o imie z klasy czy imie z metody , PIERWSZE IMIE DOTYCZY KLASY ( THIS UŻYWAMY TYLKO DO POLA KLASY)
        

    }
    public void setWiek (short wiek) { // ustawianie metod

        this.wiek = wiek;
    }

    public void setNazwisko (String nazwisko){

        this.nazwisko = nazwisko;
    }
    public void setWzrost(int wzrost) {

        this.wzrost = wzrost;
    }



    String getImie() {

    return this.imie;
    }
    String getNazwisko()
    {
        return this.nazwisko;
    }
    short getWiek()
    {
        return this.wiek;
    }
    int getWzrost() {

        return this.wzrost;
    }


    Czlowiek(String imie, String nazwisko, int wzrost, short wiek) {// konstruktow arguemntowy
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wzrost = wzrost;
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return "Czlowiek{" +
                "wiek=" + wiek +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", wzrost=" + wzrost +
                '}';
    }
}


